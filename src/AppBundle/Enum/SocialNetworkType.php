<?php
/**
 * Created by PhpStorm.
 * User: evgen
 * Date: 11.02.2018
 * Time: 17:37
 */

namespace AppBundle\Enum;


class SocialNetworkType
{
    public const VKONTAKTE = 'vk';
    public const TELEGRAM = 'tg';
}