<?php

namespace AppBundle\Exception;

use Exception;
use PHPSocketIO\SocketIO;

/**
 * Class ApiErrorException
 * @package AppBundle\Exception
 */
class ApiErrorException extends Exception
{
    public const METHOD = 'handler.error';

    /**
     * ApiErrorException constructor.
     * @param SocketIO $connection
     * @param string $message
     * @param array $trace
     */
    public function __construct(SocketIO $connection = null, string $message, array $trace)
    {
        parent::__construct();

        dump($message, reset($trace));

        if ($connection instanceof SocketIO) {
            $connection->emit(self::METHOD, json_encode([
                'status' => 'error',
                'description' => $message,
            ]));
        }
    }
}
