<?php
/**
 * Created by PhpStorm.
 * User: evgen
 * Date: 15.05.17
 * Time: 16:38
 */

namespace AppBundle\Entity;

use AppBundle\Traits\AtTime;
use AppBundle\Traits\Identifier;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="device")
 * @ORM\HasLifecycleCallbacks
 */
class Device
{
    use Identifier;
    use AtTime;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    private $uuid;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="Token", mappedBy="device", cascade={"persist", "remove"})
     */
    private $tokenList;

    public function __construct()
    {
        $this->tokenList = new ArrayCollection();
    }

    /**
     * @param string $uuid
     * @return Device
     */
    public function setUuid(string $uuid): Device
    {
        $this->uuid = $uuid;
        return $this;
    }

    /**
     * @return string
     */
    public function getUuid(): string
    {
        return $this->uuid;
    }

    /**
     * @param Token $token
     * @return Device
     */
    public function addToken(Token $token): self
    {
        $this->tokenList[] = $token;
        return $this;
    }

    /**
     * @param Token $token
     * @return Device
     */
    public function removeToken(Token $token): self
    {
        $this->tokenList->removeElement($token);
        return $this;
    }

    /**
     * @return Collection
     */
    public function getTokenList(): Collection
    {
        return $this->tokenList;
    }
}
