<?php
/**
 * Created by PhpStorm.
 * User: evgen
 * Date: 15.05.17
 * Time: 16:38
 */

namespace AppBundle\Entity;

use AppBundle\Traits\AtTime;
use AppBundle\Traits\Identifier;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="token")
 * @ORM\HasLifecycleCallbacks
 */
class Token
{
    use Identifier;
    use AtTime;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    private $token;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    private $type;

    /**
     * @var Device
     * @ORM\ManyToOne(targetEntity="Device", inversedBy="tokenList", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="device_id", referencedColumnName="id")
     */
    private $device;

    /**
     * @param string $type
     * @return Token
     */
    public function setType(string $type): Token
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $token
     * @return Token
     */
    public function setToken(string $token): Token
    {
        $this->token = $token;
        return $this;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @param Device $device
     * @return Token
     */
    public function setDevice(Device $device): self
    {
        $this->device = $device;
        return $this;
    }

    /**
     * @return Device
     */
    public function getDevice(): Device
    {
        return $this->device;
    }
}
