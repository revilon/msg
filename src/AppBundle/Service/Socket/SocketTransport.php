<?php

namespace AppBundle\Service\Socket;

/**
 * Class SocketTransport
 * @package AppBundle\Common
 */
class SocketTransport
{
    /** @var resource */
    private $socket;

    /**
     * @param int $domain
     * @param int $type
     * @param int $protocol
     * @return SocketTransport
     */
    public function create(int $domain, int $type, int $protocol): self
    {
        $this->socket = socket_create($domain, $type, $protocol);

        return $this;
    }

    public function __destruct()
    {
        socket_close($this->socket);
    }

    /**
     * @param int $level
     * @param int $name
     * @param $value
     * @return bool
     */
    public function setOption(int $level, int $name, $value): bool
    {
        if (in_array($name, [\SO_RCVTIMEO, \SO_SNDTIMEO])) {
            $value = ['sec' => (int) $value, 'usec' => (int) (($value - (int) $value) * 1000000)];
        }

        return socket_set_option($this->socket, $level, $name, $value);
    }

    /**
     * @param int $level
     * @param int $name
     * @return mixed
     */
    public function getOption(int $level, int $name)
    {
        return socket_get_option($this->socket, $level, $name);
    }

    /**
     * @param bool $blocking
     * @return bool
     */
    public function setBlocking(bool $blocking): bool
    {
        if ($blocking) {
            return socket_set_block($this->socket);
        }

        return socket_set_nonblock($this->socket);
    }

    /**
     * @param string $address
     * @param int $port
     * @return bool
     */
    public function bind(string $address, int $port = 0): bool
    {
        return socket_bind($this->socket, $address, $port);
    }

    /**
     * @param int $backlog
     * @return bool
     */
    public function listen(int $backlog = 0): bool
    {
        return socket_listen($this->socket, $backlog);
    }

    /**
     * @return resource
     */
    public function accept()
    {
        return socket_accept($this->socket);
    }

    /**
     * @param int $timeout
     * @return bool|resource
     */
    public function acceptStream($timeout = -1)
    {
        return stream_socket_accept($this->socket, $timeout);
    }

    /**
     * @param string $address
     * @param int $port
     * @return bool
     */
    public function connect(string $address, int $port = 0): bool
    {
        return socket_connect($this->socket, $address, $port);
    }

    /**
     * @param array $read
     * @param array $write
     * @param array $except
     * @param int $tvSec
     * @param int $tvUsec
     * @return int
     */
    public function select(array &$read, array &$write, array &$except, int $tvSec, int $tvUsec = 0): int
    {
        return socket_select($read, $write, $except, $tvSec, $tvUsec);
    }

    /**
     * @param $connect
     * @param int $length
     * @param int $flags
     * @return string
     */
    public function read($connect, int $length, int $flags = 0)
    {
        if ($connect instanceof self) {
            $connect = $connect->getSocket();
        }

        return socket_read($connect, $length, $flags);
    }

    /**
     * @param $connect
     * @param string $buffer
     * @param int $length
     * @return int
     */
    public function write($connect, string $buffer, int $length = -1): int
    {
        if ($connect instanceof self) {
            $connect = $connect->getSocket();
        }

        if ($length === -1) {
            $result = socket_write($connect, $buffer);
        } else {
            $result = socket_write($connect, $buffer, $length);
        }

        return $result;
    }

    /**
     * @param string $data
     * @param int $length
     * @param int $flags
     * @return int
     */
    public function send(string $data, int $length, int $flags): int
    {
        return socket_send($this->socket, $data, $length, $flags);
    }

    /**
     * @return void
     */
    public function close(): void
    {
        socket_close($this->socket);
    }

    /**
     * @return array
     */
    public function getPeerName(): array
    {
        socket_getpeername($this->socket, $address, $port);

        return $port ? ['host' => $address, 'port' => $port] : ['host' => $address];
    }

    /**
     * @return array
     */
    public function getSockName(): array
    {
        socket_getsockname($this->socket, $address, $port);

        return $port ? ['host' => $address, 'port' => $port] : ['host' => $address];
    }

    /**
     * @param resource $connect
     */
    public function shutdown($connect): void
    {
        socket_shutdown($connect);
    }

    /**
     * @return resource
     */
    public function getSocket()
    {
        return $this->socket;
    }

    public function handshake($connect) {
        $info = array();

        $line = fgets($connect);
        $header = explode(' ', $line);
        $info['method'] = $header[0];
        $info['uri'] = $header[1];

        //считываем заголовки из соединения
        while ($line = rtrim(fgets($connect))) {
            if (preg_match('/\A(\S+): (.*)\z/', $line, $matches)) {
                $info[$matches[1]] = $matches[2];
            } else {
                break;
            }
        }

        $address = explode(':', stream_socket_get_name($connect, true)); //получаем адрес клиента
        $info['ip'] = $address[0];
        $info['port'] = $address[1];

        if (empty($info['Sec-WebSocket-Key'])) {
            return false;
        }

        //отправляем заголовок согласно протоколу вебсокета
        $SecWebSocketAccept = base64_encode(pack('H*', sha1($info['Sec-WebSocket-Key'] . '258EAFA5-E914-47DA-95CA-C5AB0DC85B11')));
        $upgrade = "HTTP/1.1 101 Web Socket Protocol Handshake\r\n" .
            "Upgrade: websocket\r\n" .
            "Connection: Upgrade\r\n" .
            "Sec-WebSocket-Accept:$SecWebSocketAccept\r\n\r\n";
        fwrite($connect, $upgrade);

        return $info;
    }
}
