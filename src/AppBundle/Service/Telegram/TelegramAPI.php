<?php

namespace AppBundle\Service\Telegram;

use danog\MadelineProto\API;
use danog\MadelineProto\updates;

/**
 * Class TelegramAPI
 * @package AppBundle\Service\Telegram
 */
class TelegramAPI extends API
{
    /**
     * @param string $phone
     * @return mixed
     */
    public function phoneLogin(string $phone)
    {
        return parent::phone_login($phone);
    }

    /**
     * @param string $code
     * @return mixed
     */
    public function completePhoneLogin(string $code)
    {
        return parent::complete_phone_login($code);
    }

    /**
     * @return mixed
     */
    public function getDialogs()
    {
        return parent::get_dialogs();
    }

    /**
     * @param $peerId
     * @return mixed
     */
    public function getFullInfo($peerId)
    {
        return parent::get_full_info($peerId);
    }

    /**
     * @param $peer - int / @nickname
     * @param string $message
     * @return updates
     */
    public function sendMessage($peer, string $message)
    {
        return $this->messages->sendMessage([
            'peer' => $peer,
            'message' => $message,
        ]);
    }

    public function getUserPhoto($userId)
    {
        $photos = $this->photos->getUserPhotos([
            'user_id' => $userId,
            'offset' => 0,
            'max_id' => 0,
            'limit' => 0,
        ]);

        if (empty($photos['photos'][0])) {
            return [];
        }

        return $photos['photos'][0];
    }

    /**
     * @param array $messageMediaPhoto
     * @param string $directory
     * @return string - server file path
     */
    public function downloadToDir(array $messageMediaPhoto, string $directory): string
    {
        return $this->download_to_dir($messageMediaPhoto, $directory);
    }

    /**
     * @param array $messageMediaPhoto
     * @return array
     */
    public function getDownloadInfo(array $messageMediaPhoto): array
    {
        return $this->get_download_info($messageMediaPhoto);
    }

    /**
     * @param int $updateId
     * @return array
     */
    public function longPoll(int $updateId): array
    {
        return $this->API->get_updates([
            'offset' => $updateId,
            'limit' => 50,
            'timeout' => 1
        ]);
    }
}