<?php
/**
 * Created by PhpStorm.
 * User: evgen
 * Date: 16.12.2017
 * Time: 21:42
 */

namespace AppBundle\Service\Telegram;

use Symfony\Component\DependencyInjection\ContainerInterface;

class TelegramService
{
    /** @var ContainerInterface */
    private $container;

    /** @var string */
    private $token;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }
}