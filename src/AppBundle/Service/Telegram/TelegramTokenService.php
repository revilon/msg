<?php
/**
 * Created by PhpStorm.
 * User: evgen
 * Date: 16.12.2017
 * Time: 20:28
 */

namespace AppBundle\Service\Telegram;

use AppBundle\Entity\Device;
use AppBundle\Entity\Token;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;
use Goutte\Client;
use Symfony\Component\HttpKernel\EventListener\ResponseListener;

/**
 * https://my.telegram.org/apps
 *
 * Class VkTokenService
 * @package AppBundle\Service
 */
class TelegramTokenService
{
    const API_URL = 'https://oauth.vk.com/authorize';
    const CLIENT_ID = '6273097';
    const REDIRECT_URI = 'https://oauth.vk.com/blank.html';
    const DISPLAY = 'page';
    const RESPONSE_TYPE = 'token';
    const API_VERSION = '5.69';
    const SCOPE = 'messages,friends,offline,notifications,email';

    /** @var Session */
    private $session;

    /** @var Registry */
    private $doctrine;

    /** @var string */
    private $login;

    /** @var string */
    private $password;

    /** @var string */
    private $token;

    /** @var RequestStack */
    private $requestStack;

    public function __construct(Session $session, Registry $doctrine, RequestStack $requestStack)
    {
        $this->session = $session;
        $this->doctrine = $doctrine;
        $this->requestStack = $requestStack;
    }

    /**
     * 'email' => 'revilon@yandex.ru',
     * 'pass' => 'Kuzya911Gfhjkm911',
     * 'email' => 'revil-on@mail.ru',
     * 'pass' => 'utihot62',
     * @throws \Exception
     * @return void
     */
    final public function buildToken(): void
    {
        $paramList = [
            'client_id' => self::CLIENT_ID,
            'redirect_uri' => self::REDIRECT_URI,
            'display' => self::DISPLAY,
            'response_type' => self::RESPONSE_TYPE,
            'v' => self::API_VERSION,
            'scope' => self::SCOPE,
            'state' => 'getToken',
        ];
        $query = sprintf(
            '%s?%s',
            self::API_URL,
            http_build_query($paramList)
        );

        try {
            $client = new Client();
            $crawler = $client->request('GET', $query);

            $loginForm = $crawler->filter('form [type=submit]')->form();
            $crawler = $client->submit($loginForm, [
                'email' => $this->login,
                'pass' => $this->password,
            ]);

            $grantAccess = $crawler->filter('form[action*="https://login.vk.com/?act=grant_access"]');
            if ($grantAccess->getNode(0)) {
                $grantAccessForm = $grantAccess->form();
                $crawler = $client->submit($grantAccessForm);
            }

            $tokenString = parse_url(
                $crawler->getBaseHref(),
                PHP_URL_FRAGMENT
            );
            parse_str($tokenString, $output);

            [
                'access_token' => $accessToken,
                'user_id' => $userId,
                'expires_in' => $expires,
                'state' => $state,
            ] = $output;
            $this->setToken($accessToken);
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @param string $token
     * @return TelegramTokenService
     */
    public function setToken(string $token): TelegramTokenService
    {
        $this->token = $token;
        return $this;
    }

    /**
     * 'email' => 'revilon@yandex.ru',
     * 'pass' => 'Kuzya911Gfhjkm911',
     * 'email' => 'revil-on@mail.ru',
     * 'pass' => 'utihot62',
     * @throws \Exception
     * @return void
     */
    final public function buildToken2(): void
    {
        $this->session->remove('vk_access_token');
        $this->session->remove('vk_user_id');

        $paramList = [
            'client_id' => self::CLIENT_ID,
            'redirect_uri' => self::REDIRECT_URI,
            'display' => self::DISPLAY,
            'response_type' => self::RESPONSE_TYPE,
            'v' => self::API_VERSION,
            'scope' => self::SCOPE,
            'state' => 'getToken',
        ];
        $query = sprintf(
            '%s?%s',
            self::API_URL,
            http_build_query($paramList)
        );

        try {
//            $client = new Client();
//            $crawler = $client->request('GET', $query);
//
//            $loginForm = $crawler->filter('form [type=submit]')->form();
//            $crawler = $client->submit($loginForm, [
//                'email' => $this->login,
//                'pass' => $this->password,
//            ]);
//
//            $grantAccess = $crawler->filter('form[action*="https://login.vk.com/?act=grant_access"]');
//            if ($grantAccess->getNode(0)) {
//                $grantAccessForm = $grantAccess->form();
//                $crawler = $client->submit($grantAccessForm);
//            }
//
//            $tokenString = parse_url(
//                $crawler->getBaseHref(),
//                PHP_URL_FRAGMENT
//            );
//            parse_str($tokenString, $output);
//
//            [
//                'access_token' => $accessToken,
//                'user_id' => $userId,
//                'expires_in' => $expires,
//                'state' => $state,
//            ] = $output;
            $accessToken = 'f9a0b65e152a4c7fcefee30fabe509b55790a34dd3cdd18bd871150ea36accffd315be79eb765f6848acb';
            $this->setToken($accessToken);
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @param string $login
     * @return VkTokenService
     */
    public function setLogin(string $login): VkTokenService
    {
        $this->login = $login;
        return $this;
    }

    /**
     * @param string $password
     * @return VkTokenService
     */
    public function setPassword(string $password): VkTokenService
    {
        $this->password = $password;
        return $this;
    }
}
