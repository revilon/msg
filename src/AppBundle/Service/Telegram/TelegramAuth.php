<?php

namespace AppBundle\Service\Telegram;

use Dotenv\Dotenv;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class TelegramAuth
 * @package AppBundle\Service\Telegram
 */
class TelegramAuth
{
    /** @var string */
    private $session;

    /** @var ContainerInterface */
    private $container;

    /**
     * TelegramAuth constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param $phone
     * @throws \Exception
     */
    public function authorization($phone)
    {
        $configPath = $this->container->getParameter('kernel.project_dir') . '/app/config';
        $envFile = 'telegram.env';

        if (!file_exists($configPath . '/' . $envFile)) {
            throw new \Exception(sprintf('file "%s/%s" not exists', $configPath, $envFile));
        }

        $dotEnv = new Dotenv($configPath, $envFile);
        $dotEnv->load();

        $settings = json_decode(getenv('MTPROTO_SETTINGS'), true) ?: [];
        $settings['connection']['main']['ipv4'][2]['ip_address'] = '149.154.167.50';
        $settings['connection']['test']['ipv4'][2]['ip_address'] = '149.154.167.40';
        $settings['peer']['full_fetch'] = true;
        $settings['peer']['cache_all_peers_on_startup'] = true;
        $settings['pwr']['requests'] = false;
        $settings['updates']['handle_old_updates'] = false;
//        $settings['logger']['logger_level'] = Logger::ULTRA_VERBOSE;

        $telegramAPI = new TelegramAPI($settings);
        $telegramAPI->session = $this->session;

        $telegramAPI->phoneLogin($phone);

        $telegramAPI->serialize($this->session);
    }

    /**
     * @param $code
     */
    public function completeAuthorization($code)
    {
        $telegramAPI = new TelegramAPI($this->session);

        $telegramAPI->completePhoneLogin($code);

        $telegramAPI->serialize($this->session);
    }

    /**
     * @param string $session
     * @return TelegramAuth
     */
    public function setSession(string $session): TelegramAuth
    {
        $this->session = $session;
        return $this;
    }
}