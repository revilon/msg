<?php

namespace AppBundle\Service\Vk;

use AppBundle\Dto\BaseQueryDto;
use AppBundle\Exception\ApiErrorException;
use AppBundle\Service\Vk\Login\LoginFormatter;
use AppBundle\Service\Vk\Login\LoginHandler;
use AppBundle\Service\Vk\Login\LoginQuery;
use PHPSocketIO\Engine\Engine;
use PHPSocketIO\Nsp;
use PHPSocketIO\Socket;
use PHPSocketIO\SocketIO;
use PHPSocketIO\Engine\Socket as SocketClient;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class MethodSchema
 * @package AppBundle\Service\Vk
 */
class VkMethodRouter
{
    public const SMS = 'sms';
    public const CAPTCHA = 'captcha';
    public const SEND_MESSAGE = 'send_message';
    public const FRIEND_LIST = 'friend_list';
    public const CHECK_TOKEN = 'check_token';
    public const TAKE_TOKEN = 'take_token';

    /** @var ContainerInterface */
    protected $container;

    /**
     * MethodRouter constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param BaseQueryDto $baseQuery
     * @return void
     */
    public function route(BaseQueryDto $baseQuery): void
    {
        switch ($baseQuery->getMethod()) {
            case LoginQuery::METHOD:
                $handler = $this->container->get(LoginHandler::class);
                $query = $this->container->get(LoginQuery::class);
                $formatter = $this->container->get(LoginFormatter::class);

                $query->resolve($baseQuery->getQuery());

                $data = $handler->handle($query);
                $response = $formatter->format($data);

                $baseQuery->getConnection()->emit($baseQuery->getMethod(), json_encode($response));

                break;
        }
    }

    /**
     * @param Socket $socket
     * @param SocketIO $io
     * @return void
     */
    public function initMethodList(Socket $socket, SocketIO $io): void
    {
        $methodList = [
            LoginQuery::METHOD,
        ];

        foreach ($methodList as $method) {
            $socket->on($method, function (array $request) use ($io, $method) {
                /** @var Nsp $root */
                $root = $io->nsps['/'];
                /** @var SocketIO $server */
                $server = $root->server;
                /** @var Engine $engine */
                $engine = $server->engine;
                /** @var SocketClient[] $clientList */
                $clientList = $engine->clients;

                foreach ($clientList as $sid => $client) {
                    try {
                        $baseQueryDto = new BaseQueryDto([
                            'connection' => $io,
                            'method' => $method,
                            'headers' => $client->request->headers,
                            'query' => $request,
                        ]);
                        $methodRouter = $this->container->get(VkMethodRouter::class);
                        $methodRouter->route($baseQueryDto);
                    } catch (\Exception $exception) {
                        throw new ApiErrorException(
                            $io,
                            $exception->getMessage(),
                            $exception->getTrace()
                        );
                    }
                }
            });
        }
    }
}
