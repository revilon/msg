<?php
/**
 * Created by PhpStorm.
 * User: evgen
 * Date: 16.12.2017
 * Time: 20:28
 */

namespace AppBundle\Service\Vk;

use Doctrine\Bundle\DoctrineBundle\Registry;
use DOMElement;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;
use Goutte\Client;
use \JonnyW\PhantomJs\Client as PhantomJs;

/**
 * https://vk.com/editapp?id=6273097&section=options
 *
 * Class VkTokenService
 * @package AppBundle\Service
 */
class VkTokenService
{
    const API_URL = 'https://oauth.vk.com/authorize';
    const CLIENT_ID = '6273097';
    const REDIRECT_URI = 'https://oauth.vk.com/blank.html';
    const DISPLAY = 'page';
    const RESPONSE_TYPE = 'token';
    const API_VERSION = '5.69';
    const SCOPE = 'messages,friends,offline,notifications,email';

    const MIGRATION_DATA_NAME = 'migrate_data_vk';

    /** @var Session */
    private $session;

    /** @var Registry */
    private $doctrine;

    /** @var string */
    private $login;

    /** @var string */
    private $password;

    /** @var string */
    private $token;

    /** @var string */
    private $migrateDataVk;

    /** @var RequestStack */
    private $requestStack;

    /** @var Client */
    private $client;

    /** @var array */
    public $statusRequest = [];

    public function __construct(Session $session, Registry $doctrine, RequestStack $requestStack)
    {
        $this->session = $session;
        $this->doctrine = $doctrine;
        $this->requestStack = $requestStack;

        $this->client = new Client();
        $this->client->setHeader('accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8');
        $this->client->setHeader('accept-encoding', 'gzip, deflate, br');
        $this->client->setHeader('accept-language', 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7');
        $this->client->setHeader('upgrade-insecure-requests', '1');
        $this->client->setHeader('user-agent', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36');
    }

    /**
     * 'email' => 'revilon@yandex.ru',
     * 'pass' => 'Kuzya911Gfhjkm911',
     * 'email' => 'revil-on@mail.ru',
     * 'pass' => 'utihot62',
     * @throws \Exception
     */
    final public function buildToken(): void
    {
        try {
            $this->buildRequest();
            if ($this->smsCheck()) {
                return;
            }

            $this->grantAccess();

            $this->grabToken();
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * 'email' => 'revilon@yandex.ru',
     * 'pass' => 'Kuzya911Gfhjkm911',
     * 'email' => 'revil-on@mail.ru',
     * 'pass' => 'utihot62',
     * @param array $smsParamList
     * @throws \Exception
     */
    final public function buildTokenAfterSmsCheck(array $smsParamList): void
    {
        $client = $this->getClient();

        try {
            $query = $smsParamList[self::MIGRATION_DATA_NAME]['action'];
            $bodyPost = $smsParamList[self::MIGRATION_DATA_NAME]['values'];
            $cookies = $smsParamList[self::MIGRATION_DATA_NAME]['cookies'];

            /** @var \Symfony\Component\BrowserKit\Cookie[] $cookieJarArray */
            foreach ($cookies as $cookie) {
                $client->getCookieJar()->set($cookie);
            }

            $client->request('POST', $query);
            $smsCheckerForm = $client->getCrawler()->filter('form[action*="/login?act=authcheck_code"]')->form();
            unset($bodyPost['captcha_sid']);
            unset($bodyPost['captcha_key']);
            $client->submit($smsCheckerForm, $bodyPost);

            if ($this->checkCaptcha()) {
                return;
            }

            $this->grantAccess();
            $this->grabToken();
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * 'email' => 'revilon@yandex.ru',
     * 'pass' => 'Kuzya911Gfhjkm911',
     * 'email' => 'revil-on@mail.ru',
     * 'pass' => 'utihot62',
     * @param array $migrationData
     * @throws \Exception
     */
    final public function buildTokenAfterCaptchaCheck(array $migrationData): void
    {
        $client = $this->getClient();

        try {
            $query = $migrationData[self::MIGRATION_DATA_NAME]['action'];
            $bodyPost = $migrationData[self::MIGRATION_DATA_NAME]['values'];
            $cookies = $migrationData[self::MIGRATION_DATA_NAME]['cookies'];

            /** @var \Symfony\Component\BrowserKit\Cookie[] $cookieJarArray */
            foreach ($cookies as $cookie) {
                $client->getCookieJar()->set($cookie);
            }

            $client->request('POST', $query, $bodyPost);

            $this->grantAccess();
            $this->grabToken();
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    private function checkCaptcha(): bool
    {
        $client = $this->getClient();

        $captchaForm = $client->getCrawler()->filter('form');

        if (!$captchaForm->getNode(0)) {
            return false;
        }

        $captchaForm = $captchaForm->form();
        $captchaFormNode = $captchaForm->getNode();
        $isCaptcha = false;
        foreach ($captchaFormNode->childNodes as $childNode) {
            if ($childNode instanceof DOMElement) {
                switch ($childNode->getAttribute('name')) {
                    case 'captcha_sid':
                        $isCaptcha = true;
                        break(2);
                }
            }
        }

        if ($isCaptcha) {
            $clientCaptcha = new Client();
            $imageCaptcha = $client->getCrawler()->filter('.captcha_img')->image();
            $clientCaptcha->request($imageCaptcha->getMethod(), $imageCaptcha->getUri());
            /** @var \Symfony\Component\BrowserKit\Response $response */
            $response = $clientCaptcha->getResponse();

            $paramList = serialize([
                self::MIGRATION_DATA_NAME => [
                    'action' => $captchaForm->getUri(),
                    'method' => $captchaForm->getMethod(),
                    'values' => $captchaForm->getValues(),
                    'cookies' => $client->getCookieJar()->all(),
                ]
            ]);

            $gz = base64_encode($paramList);
            $randomCharPosition = random_int(1, strlen($gz));
            $randomChar = substr($gz, $randomCharPosition - 1, 1);
            $gzModify = $randomChar . $gz;

            $this->statusRequest = [
                'status' => 'captcha_checker',
                'image_base64_captcha' => base64_encode($response->getContent()),
            ];

            $this->setMigrateDataVk($gzModify);
        }

        return $this->getMigrateDataVk() ? true : false;
    }

    private function buildRequest(): void
    {
        $client = $this->getClient();

        $paramList = [
            'client_id' => self::CLIENT_ID,
            'redirect_uri' => self::REDIRECT_URI,
            'display' => self::DISPLAY,
            'response_type' => self::RESPONSE_TYPE,
            'v' => self::API_VERSION,
            'scope' => self::SCOPE,
            'state' => 'getToken',
        ];
        $query = sprintf(
            '%s?%s',
            self::API_URL,
            http_build_query($paramList)
        );

        $client->request('GET', $query);

        // авторизация
        $loginForm = $client->getCrawler()->filter('#login_submit')->form();
        $client->submit($loginForm, [
            'email' => $this->login,
            'pass' => $this->password,
        ]);
        // https://oauth.vk.com/error?err=2
        // Тут проблема на стороне вк, слишком много раз запросили новый токен
        // Тепрь ждать сутки пока сессия у вк пропадет на этот аккаунт
//        dd($client->getRequest());
//        die;
    }

    private function smsCheck(): bool
    {
        $client = $this->getClient();

        $helpCheckerButton = $client->getCrawler()->filter('a[href*="help_opened"]');

        if ($helpCheckerButton->getNode(0)) {
            $helpCheckerLink = $helpCheckerButton->link();
            $client->click($helpCheckerLink);

            // Иногда хрень и он берет форму не правильную или правильную
            // Кароче он потом смску отправляет в форму куда отправлять пароль из приложения auth на телефоне
            // фиксится чисткой куков на клиенте, конкретно сессии
            $smsCheckerButton = $client->getCrawler()->filter('a[href*="/login?act=authcheck_sms"]');
            $smsCheckerLink = $smsCheckerButton->link();
            $client->click($smsCheckerLink);
            $smsCheckerForm = $client->getCrawler()->filter('form[action*="/login?act=authcheck_code"]')->form();

            $paramList = serialize([
                self::MIGRATION_DATA_NAME => [
                    'action' => $smsCheckerForm->getUri(),
                    'method' => $smsCheckerForm->getMethod(),
                    'values' => $smsCheckerForm->getValues(),
                    'cookies' => $client->getCookieJar()->all(),
                ]
            ]);
            $gz = base64_encode($paramList);
            $randomCharPosition = random_int(1, strlen($gz));
            $randomChar = substr($gz, $randomCharPosition - 1, 1);
            $gzModify = $randomChar . $gz;

            $this->statusRequest = [
                'status' => 'sms_checker',
            ];
            $this->setMigrateDataVk($gzModify);
        }

        return $this->getMigrateDataVk() ? true : false;
    }

    private function grantAccess(): void
    {
        $client = $this->getClient();

        // получение прав доступа для приложения
        $grantAccess = $client->getCrawler()->filter('form[action*="login.vk.com/?act=grant_access"]');
        if ($grantAccess->getNode(0)) {
            $grantAccessForm = $grantAccess->form();
            $client->submit($grantAccessForm);
        }

        // получение прав доступа для приложения
        $grantAccess = $client->getCrawler()->filter('button[onclick*="allow(this)"]');
        if ($grantAccess->getNode(0)) {
            preg_match_all(
                '#(https:\/\/login\.vk\.com\/\?act=grant_access&client_id=.*)\"\+#',
                $client->getCrawler()->html(),
                $matches
            );
            $url = $matches[1][0];
            $url .= '&notify=1';
            $client->request('GET', $url);
        }
    }

    private function grabToken(): void
    {
        $client = $this->getClient();

        $tokenString = parse_url(
            $client->getCrawler()->getBaseHref(),
            PHP_URL_FRAGMENT
        );

        parse_str($tokenString, $output);
        [
            'access_token' => $accessToken,
            'user_id' => $userId,
            'expires_in' => $expires,
            'state' => $state,
        ] = $output;

        $this->setToken($accessToken);
    }

    /**
     * @param string $login
     * @return VkTokenService
     */
    public function setLogin(string $login): VkTokenService
    {
        $this->login = $login;
        return $this;
    }

    /**
     * @param string $password
     * @return VkTokenService
     */
    public function setPassword(string $password): VkTokenService
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @param string $migrateDataVk
     * @return VkTokenService
     */
    public function setMigrateDataVk(string $migrateDataVk): VkTokenService
    {
        $this->migrateDataVk = $migrateDataVk;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getMigrateDataVk(): ?string
    {
        return $this->migrateDataVk;
    }

    /**
     * @param Client $client
     * @return VkTokenService
     */
    public function setClient(Client $client): VkTokenService
    {
        $this->client = $client;
        return $this;
    }

    /**
     * @return Client
     */
    public function getClient(): Client
    {
        return $this->client;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @param string $token
     * @return VkTokenService
     */
    public function setToken(string $token): VkTokenService
    {
        $this->token = $token;
        return $this;
    }
}
