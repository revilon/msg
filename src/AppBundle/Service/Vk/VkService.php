<?php
/**
 * Created by PhpStorm.
 * User: evgen
 * Date: 16.12.2017
 * Time: 21:42
 */

namespace AppBundle\Service\Vk;

use Symfony\Component\DependencyInjection\ContainerInterface;

class VkService
{
    /** @var ContainerInterface */
    private $container;

    /** @var string */
    private $token;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param string $id
     * @param string $message
     * @return string
     */
    public function sendMessage(string $id, string $message): string
    {
        $url = 'https://api.vk.com/method/messages.send';
        $params = [
            'user_id' => $id,    // Кому отправляем
            'message' => $message,   // Что отправляем
            'access_token' => $this->getToken(),
            'v' => '5.69',
        ];

        // В $result вернется id отправленного сообщения
        $result = file_get_contents($url, false, stream_context_create([
            'http' => [
                'method'  => 'POST',
                'header'  => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query($params)
            ]
        ]));

        return $result;
    }

    /**
     * @return string
     */
    public function getFriendList(): string
    {
        $url = 'https://api.vk.com/method/friends.get';
        $params = [
            'order' => 'hints',
            'fields' => 'nickname,sex,photo_200_orig,online,last_seen,can_write_private_message,domain',
            'access_token' => $this->getToken(),
            'v' => '5.69',
        ];

        // В $result вернется id отправленного сообщения
        $result = file_get_contents($url, false, stream_context_create([
            'http' => [
                'method'  => 'POST',
                'header'  => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query($params)
            ]
        ]));

        return $result;
    }


    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @param string $token
     * @return VkService
     */
    public function setToken(string $token): VkService
    {
        $this->token = $token;
        return $this;
    }
}