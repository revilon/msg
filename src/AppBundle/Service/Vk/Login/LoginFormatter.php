<?php

namespace AppBundle\Service\Vk\Login;

/**
 * Class LoginFormatter
 * @package AppBundle\Service\Vk\Login
 */
class LoginFormatter
{
    /**
     * @param array $data
     * @return array
     */
    public function format(array $data): array
    {
        return $data;
    }
}
