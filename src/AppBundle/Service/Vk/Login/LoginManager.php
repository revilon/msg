<?php

namespace AppBundle\Service\Vk\Login;

use Doctrine\ORM\EntityManager;

/**
 * Class LoginManager
 * @package AppBundle\Service\Vk\Login
 */
class LoginManager
{
    /** @var EntityManager */
    private $em;

    /**
     * LoginManager constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }


}
