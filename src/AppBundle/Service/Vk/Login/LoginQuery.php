<?php

namespace AppBundle\Service\Vk\Login;

use AppBundle\Dto\AbstractDto;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class LoginQuery
 * @package AppBundle\Service\Vk\Login
 * @method getUuid(): string
 * @method getLogin(): string
 * @method getPassword(): string
 */
class LoginQuery extends AbstractDto
{
    public const METHOD = 'vk.login';

    /**
     * @param OptionsResolver $options
     * @return void
     */
    protected function configureOptions(OptionsResolver $options): void
    {
        $options->setRequired([
            'uuid',
            'login',
            'password',
        ]);

        $options->setAllowedTypes('uuid', 'string');
        $options->setAllowedTypes('login', 'string');
        $options->setAllowedTypes('password', 'string');
    }
}
