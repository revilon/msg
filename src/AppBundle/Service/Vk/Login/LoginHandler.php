<?php

namespace AppBundle\Service\Vk\Login;

use AppBundle\Service\Vk\VkManager;
use Goutte\Client;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class LoginHandler
 * @property Client client
 * @package AppBundle\Service\Vk\Login
 */
class LoginHandler
{
    private const API_URL = 'https://oauth.vk.com/authorize';
    private const CLIENT_ID = '6273097';
    private const REDIRECT_URI = 'https://oauth.vk.com/blank.html';
    private const DISPLAY = 'page';
    private const RESPONSE_TYPE = 'token';
    private const API_VERSION = '5.69';
    private const SCOPE = 'messages,friends,offline,notifications,email';

    /**
     * MethodRouter constructor.
     * @param ContainerInterface $container
     * @param VkManager $vkManager
     */
    public function __construct(ContainerInterface $container, VkManager $vkManager)
    {
        $this->container = $container;
        $this->vkManager = $vkManager;
        $this->initClient();
    }

    /**
     * @param LoginQuery $query
     * @return array
     */
    public function handle(LoginQuery $query): array
    {
        $this->authentication($query->getLogin(), $query->getPassword());

        $isTwoFactorAuthentication = $this->client->getCrawler()->filter('a[href*="help_opened"]');
        if ($isTwoFactorAuthentication->getNode(0)) {
            $migration = $this->makeSmsMigration();
            return [
                'status' => 'sms',
                'migration' => $migration,
            ];
        }

        $this->grantAccess();
        $accessToken = $this->grabToken();

        $this->vkManager->upsertTokenAndDevice($query->getUuid(), $accessToken);

        return [
            'status' => 'success',
            'access_token' => $accessToken,
        ];
    }

    /**
     * @return string
     */
    private function makeSmsMigration(): string
    {
        $twoFactorAuthenticationButton = $this->client->getCrawler()->filter('a[href*="help_opened"]');

        $helpCheckerLink = $twoFactorAuthenticationButton->link();
        $this->client->click($helpCheckerLink);

        // Иногда хрень и он берет форму не правильную или правильную
        // Кароче он потом смску отправляет в форму куда отправлять пароль из приложения auth на телефоне
        // фиксится чисткой куков на клиенте, конкретно сессии
        $smsCheckerButton = $this->client->getCrawler()->filter('a[href*="/login?act=authcheck_sms"]');
        $smsCheckerLink = $smsCheckerButton->link();
        $this->client->click($smsCheckerLink);
        $smsCheckerForm = $this->client->getCrawler()->filter('form[action*="/login?act=authcheck_code"]')->form();

        $paramList = serialize([
            'migrate_data_vk' => [
                'action' => $smsCheckerForm->getUri(),
                'method' => $smsCheckerForm->getMethod(),
                'values' => $smsCheckerForm->getValues(),
                'cookies' => $this->client->getCookieJar()->all(),
            ]
        ]);
        $migration = base64_encode($paramList);
        $migrationLength = \strlen($migration) - 1; // without =
        $randomCharPosition = random_int(1, $migrationLength);
        $randomChar = $migration[$randomCharPosition - 1];

        return $randomChar . $migration;
    }

    /**
     * @param string $login
     * @param string $password
     * @return void
     */
    private function authentication(string $login, string $password): void
    {
        $paramList = [
            'client_id' => self::CLIENT_ID,
            'redirect_uri' => self::REDIRECT_URI,
            'display' => self::DISPLAY,
            'response_type' => self::RESPONSE_TYPE,
            'v' => self::API_VERSION,
            'scope' => self::SCOPE,
        ];
        $query = sprintf(
            '%s?%s',
            self::API_URL,
            http_build_query($paramList)
        );

        $this->client->request('GET', $query);

        // авторизация
        $loginForm = $this->client->getCrawler()->filter('#login_submit')->form();
        $this->client->submit($loginForm, [
            'email' => $login,
            'pass' => $password,
        ]);

        // https://oauth.vk.com/error?err=2
        // Тут проблема на стороне вк, слишком много раз запросили новый токен
        // Тепрь ждать сутки пока сессия у вк пропадет на этот аккаунт
//        dd($this->client->getRequest());
    }

    /**
     * @return void
     */
    private function grantAccess(): void
    {
        $grantAccess = $this->client->getCrawler()->filter('form[action*="login.vk.com/?act=grant_access"]');
        if ($grantAccess->getNode(0)) {
            $grantAccessForm = $grantAccess->form();
            $this->client->submit($grantAccessForm);
        }

        $grantAccess = $this->client->getCrawler()->filter('button[onclick*="allow(this)"]');
        if ($grantAccess->getNode(0)) {
            preg_match_all(
                '#(https:\/\/login\.vk\.com\/\?act=grant_access&client_id=.*)\"\+#',
                $this->client->getCrawler()->html(),
                $matches
            );
            $url = $matches[1][0];
            $url .= '&notify=1';
            $this->client->request('GET', $url);
        }
    }

    /**
     * @return string
     */
    private function grabToken(): string
    {
        $tokenString = parse_url(
            $this->client->getCrawler()->getBaseHref(),
            PHP_URL_FRAGMENT
        );

        parse_str($tokenString, $output);
        [
            'access_token' => $accessToken,
            'user_id' => $userId,
            'expires_in' => $expires,
        ] = $output;

        return $accessToken;
    }

    /**
     * @return void
     */
    private function initClient(): void
    {
        $this->client = new Client();

        $headerList = [
            'accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
            'accept-encoding' => 'gzip, deflate, br',
            'accept-language' => 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7',
            'upgrade-insecure-requests' => '1',
            'user-agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_2) AppleWebKit/537.36 (KHTML, like Gecko) ' .
                'Chrome/63.0.3239.84 Safari/537.36',
        ];

        foreach ($headerList as $header => $value) {
            $this->client->setHeader($header, $value);
        }
    }
}
