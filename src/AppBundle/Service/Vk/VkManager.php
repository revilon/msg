<?php
/**
 * Created by PhpStorm.
 * User: evgen
 * Date: 11.02.2018
 * Time: 16:42
 */

namespace AppBundle\Service\Vk;

use AppBundle\Entity\Device;
use AppBundle\Entity\Token;
use AppBundle\Enum\SocialNetworkType;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;

/**
 * Class VkManager
 * @package AppBundle\Service\Vk
 */
class VkManager
{
    /** @var EntityManager */
    private $em;

    /**
     * VkManager constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param string $uuidDevice
     * @param string $tokenString
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Doctrine\ORM\ORMException
     */
    public function upsertTokenAndDevice(string $uuidDevice, string $tokenString): void
    {
        $device = $this->getDeviceByUuid($uuidDevice);

        if (!$device) {
            $device = new Device();
            $device->setUuid($uuidDevice);
            $this->em->persist($device);
        }

        $token = $this->getTokenByDevice($device);

        if ($token === null) {
            $token = new Token();
            $token->setToken($tokenString);
            $token->setType(SocialNetworkType::VKONTAKTE);
            $token->setDevice($device);
        }

        if ($token instanceof Token && $token->getType() !== SocialNetworkType::VKONTAKTE) {
            $token = new Token();
            $token->setToken($tokenString);
            $token->setType(SocialNetworkType::VKONTAKTE);
            $token->setDevice($device);
        }

        if ($token->getToken() !== $tokenString) {
            $token->setToken($tokenString);
        }

        $this->em->persist($token);
        $this->em->flush();
    }

    /**
     * @param string $uuidDevice
     * @return Device|null
     */
    public function getDeviceByUuid(string $uuidDevice): ?Device
    {
        /** @var EntityRepository $deviceRepository */
        $deviceRepository = $this->em->getRepository(Device::class);

        /** @var Device|null $device */
        $device = $deviceRepository->findOneBy(['uuid' => $uuidDevice]);

        return $device;
    }

    /**
     * @param Device $device
     * @return Token|null
     */
    public function getTokenByDevice(Device $device): ?Token
    {
        /** @var Token $token */
        $token = null;
        foreach ($device->getTokenList() as $token) {
            if ($token->getType() === SocialNetworkType::VKONTAKTE) {
                break;
            }
        }

        return $token;
    }
}