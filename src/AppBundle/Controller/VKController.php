<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Device;
use AppBundle\Entity\Token;
use AppBundle\Enum\SocialNetworkType;
use AppBundle\Exception\TestException;
use AppBundle\Service\Vk\Login\LoginQuery;
use AppBundle\Service\Vk\VkManager;
use AppBundle\Service\Vk\VkService;
use AppBundle\Service\Vk\VkTokenService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class VKController extends Controller
{
    /**
     * @Route("/lol", name="lol")
     * @param Request $request
     * @return JsonResponse
     */
    public function lolAction(Request $request)
    {
        throw new \Exception('hehe');

        die;
        $paramList = serialize([
            'migrate_data_vk' => [
                'action' => 'privet',
                'method' => 'poka',
                'values' => ['lol'=>'kek', 'hah'=>'hoh'],
                'cookies' => [1,3,445,65,6,0],
            ]
        ]);
        $migration = base64_encode($paramList);
        $migrationLength = \strlen($migration) - 1; // without =
        $randomCharPosition = random_int(1, $migrationLength);
        $randomChar = substr($migration, $randomCharPosition - 1, 1);
        $randomChar2 = $migration[$randomCharPosition - 1];
        dd($migration, $randomChar, $randomChar2);
    }

    /**
     * @Route("/login/vk", name="login_vk")
     * @param Request $request
     * @return JsonResponse
     */
    public function loginAction(Request $request): JsonResponse
    {
        $response = new JsonResponse();
        $response->headers->set('Access-Control-Allow-Origin', '*');

        $uuid = $request->get('uuid');
        $login = $request->get('login');
        $password = $request->get('password');

        $vkManager = $this->get(VkManager::class);

        $vkToken = $this->get(VkTokenService::class);
        $vkToken->setLogin($login);
        $vkToken->setPassword($password);
        $vkToken->buildToken();

        if ($vkToken->statusRequest) {
            $json = array_merge($vkToken->statusRequest, [
                VkTokenService::MIGRATION_DATA_NAME => $vkToken->getMigrateDataVk()
            ]);

            $response->setData($json);
            return $response;
        }

        $vkManager->upsertTokenAndDevice($uuid, $vkToken->getToken());

        $response->setData([
            'status' => 'success',
            'token_vk' => $vkToken->getToken(),
        ]);

        return $response;
    }

    /**
     * @Route("/sms/vk", name="sms_vk")
     * @param Request $request
     * @return JsonResponse
     */
    public function smsAction(Request $request): JsonResponse
    {
        $response = new JsonResponse();

        $uuid = $request->get('uuid');
        $migrationData = $request->get('migration');

        $vkManager = $this->get(VkManager::class);
        $smsCode = $request->get('sms_code');

        if (!$migrationData) {
            $response->setData([
                'status' => 'error',
                'description' => 'Migration data empty',
            ]);
            return $response;
        }

        $smsParamList = unserialize(base64_decode(substr($migrationData, 1)));
        $smsParamList[VkTokenService::MIGRATION_DATA_NAME]['values']['code'] = $smsCode;

        $vkToken = $this->get(VkTokenService::class);
        $vkToken->buildTokenAfterSmsCheck($smsParamList);

        if ($vkToken->statusRequest) {
            $json = array_merge($vkToken->statusRequest, [
                VkTokenService::MIGRATION_DATA_NAME => $vkToken->getMigrateDataVk()
            ]);

            $response->setData($json);
            return $response;
        }

        $vkManager->upsertTokenAndDevice($uuid, $vkToken->getToken());
        $response->setData([
            'status' => 'success',
            'token_vk' => $vkToken->getToken(),
        ]);

        return $response;
    }

    /**
     * @Route("/captcha/vk", name="captcha_vk")
     * @param Request $request
     * @return JsonResponse
     */
    public function captchaAction(Request $request): JsonResponse
    {
        $response = new JsonResponse();

        $uuid = $request->get('uuid');
        $captchaCode = $request->get('captcha_code');
        $migrationData = $request->get('migration');

        $vkManager = $this->get(VkManager::class);

        if (!$migrationData) {
            $response->setData([
                'status' => 'error',
                'description' => 'Migration Data empty',
            ]);
            return $response;
        }

        $migrationData = unserialize(base64_decode(substr($migrationData, 1)));
        $migrationData[VkTokenService::MIGRATION_DATA_NAME]['values']['captcha_key'] = $captchaCode;

        $vkToken = $this->get(VkTokenService::class);
        $vkToken->buildTokenAfterCaptchaCheck($migrationData);

        $vkManager->upsertTokenAndDevice($uuid, $vkToken->getToken());

        $response->setData([
            'status' => 'success',
            'token_vk' => $vkToken->getToken(),
        ]);

        return $response;
    }

    /**
     * @Route("/send/vk", name="send_vk")
     * @param Request $request
     * @return JsonResponse
     */
    public function sendAction(Request $request): JsonResponse
    {
        $response = new JsonResponse();

        $token = $request->get('token_vk');
        $msg = $request->get('message');
        $userTo = $request->get('user_to');

        if (!$token) {
            $response->setData([
                'status' => 'error',
            ]);

            return $response;
        }

        $vk = $this->get(VkService::class);
        $vk->setToken($token);

//        http://msg.9ek.ru/send/vk?user_to=12143704&message=test_massage
//        $userTo = '12143704';
//        $msg = 'kek';

        $vk->sendMessage($userTo, $msg);

        $response->setData([
            'status' => 'success',
            'message' => $msg,
        ]);

        return $response;
    }

    /**
     * @Route("/friendList/vk", name="friend_list_vk")
     * @param Request $request
     * @return JsonResponse
     */
    public function friendListAction(Request $request): JsonResponse
    {
        $response = new JsonResponse();
        $session = $request->getSession();

//        $sessionId = $request->get('session');
//        if ($sessionId && $session->getId() !== $sessionId) {
//            $session->setId($sessionId);
//        }
//        $token = $session->get('token_vk');
        $token = $request->get('token_vk');

        if (!$token) {
            $response->setData([
                'status' => 'error',
            ]);

            return $response;
        }

        $vk = $this->get(VkService::class);
        $vk->setToken($token);

        $friendList = $vk->getFriendList();

        $response->setJson($friendList);

        return $response;
    }

    /**
     * @Route("/check_token/vk", name="check_token_vk")
     * @param Request $request
     * @return JsonResponse
     * @throws \InvalidArgumentException
     */
    public function checkTokenAction(Request $request): JsonResponse
    {
        $response = new JsonResponse();

        $token = $request->get('token_vk');

        $vk = $this->get(VkService::class);
        $vk->setToken($token);

        $friendList = $vk->getFriendList();

        $status = json_decode($friendList);

        if (!empty($status->error)) {
            $response->setData([
                'status' => 'error',
                'description' => 'token die',
            ]);

            return $response;
        }

        $response->setJson($friendList);

        if (!$token) {
            $response->setData([
                'status' => 'error',
                'description' => 'token empty',
            ]);

            return $response;
        }

        if (!empty($status->response)) {
            $response->setData([
                'status' => 'success',
            ]);

            return $response;
        }

        $response->setData([
            'status' => 'error',
            'description' => 'undefined error',
        ]);

        return $response;
    }

    /**
     * @Route("/get_token/vk", name="get_token_vk")
     * @param Request $request
     * @return JsonResponse
     * @throws \InvalidArgumentException
     */
    public function getTokenAction(Request $request): JsonResponse
    {
        $response = new JsonResponse();

        $vkManager = $this->get(VkManager::class);

        $uuid = $request->get('uuid');

        $device = $vkManager->getDeviceByUuid($uuid);

        if ($device instanceof Device) {
            $token = $vkManager->getTokenByDevice($device);
        } else {
            $response->setData([
                'status' => 'error',
                'description' => sprintf('device uuid: "%s" not found', $uuid),
            ]);

            return $response;
        }

        if ($token instanceof Token) {
            $response->setData([
                'status' => 'success',
                'token' => $token->getToken(),
            ]);

            return $response;
        }

        $response->setData([
            'status' => 'error',
            'description' => sprintf('token: "%s" not found', SocialNetworkType::VKONTAKTE),
        ]);

        return $response;
    }
}
