<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Device;
use AppBundle\Entity\Token;
use AppBundle\Service\Socket\SocketTransport;
use AppBundle\Service\Telegram\Client;
use AppBundle\Service\TelegramService;
use AppBundle\Service\TelegramTokenService;
use AppBundle\Service\VkService;
use AppBundle\Service\VkTokenService;
use Doctrine\ORM\EntityRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class TelegramController extends Controller
{
    /**
     * @Route("/login/telegram", name="login_telegram")
     * @param Request $request
     * @return JsonResponse
     */
    public function loginAction(Request $request): JsonResponse
    {
    }

    /**
     * http://msg.9ek.ru/send/telegram?uuid=lol-kek-hah&message=dorou&user_to=317743556
     * @Route("/send/telegram", name="send_telegram")
     * @param Request $request
     * @return JsonResponse
     */
    public function sendAction(Request $request): JsonResponse
    {
        $response = new JsonResponse();

        $uuid = $request->get('uuid');
        $msg = $request->get('message');
        $userTo = $request->get('user_to');

        if (!$uuid) {
            $response->setData([
                'status' => 'error',
                'description' => 'empty uuid'
            ]);

            return $response;
        }

        $telegramStorageDirectory = $this->getParameter('telegram_storage_directory');
        $sessionStorage = sprintf('%s/%s', $telegramStorageDirectory, $uuid);
        $address = $sessionStorage . '/nasty.madeline.sock';
        $socket = new SocketTransport();
        $socket->create(AF_UNIX, SOCK_STREAM, 0);
        $socket->connect($address);
        $socket->write(
            $socket,
            json_encode([
                'action' => 'message.send',
                'body' => [
//                    'peerId' => '317743556',
//                    'message' => 'Это механическая Настя. Евгений механизировал этот аккаунт через telegram API (MTProto)',
                    'peerId' => $userTo,
                    'message' => $msg,
                ],
            ])
        );
        $result = '';
        while($read = $socket->read($socket, 1024)) {
            $result .= $read;
        }

        $response->setJson($result);

        return $response;
    }

    /**
     * @Route("/friendList/telegram", name="friend_list_telegram")
     * @param Request $request
     * @return JsonResponse
     */
    public function friendListAction(Request $request): JsonResponse
    {
        $response = new JsonResponse();

        $token = $request->cookies->get('token_vk');

        if (!$token) {
            $response->setData([
                'status' => 'error',
            ]);

            return $response;
        }

        $vk = $this->get(VkService::class);
        $vk->setToken($token);

        $friendList = $vk->getFriendList();

        $response->setJson($friendList);

        return $response;
    }
}
