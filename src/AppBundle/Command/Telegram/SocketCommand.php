<?php

namespace AppBundle\Command\Telegram;

use AppBundle\Service\Socket\SocketTransport;
use AppBundle\Service\Telegram\TelegramAPI;
use AppBundle\Service\Telegram\TelegramAuth;
use danog\MadelineProto\API;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class SocketCommand
 * @package AppBundle\Command\Telegram
 */
class SocketCommand extends ContainerAwareCommand
{
    public const RESPONSE_ERROR = 'error';
    public const RESPONSE_SUCCESS = 'success';

    protected function configure()
    {
        $this
            ->setName('tg:server:start')
            ->addArgument('uuid', InputArgument::REQUIRED, 'uuid device.')
            ->setDescription('Server telegram.')
            ->setHelp('This create loop and listen socket...')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $projectDir = $this->getContainer()->getParameter('kernel.project_dir');
        $telegramImagesUrl = $this->getContainer()->getParameter('telegram_images_url');
        $telegramImagesPath = $this->getContainer()->getParameter('telegram_images_path');
        $uuid = $input->getArgument('uuid');

        $telegramStorageDirectory = sprintf(
            '%s/var/telegram/%s',
            $projectDir,
            $uuid
        );

        $telegramAPI = new TelegramAPI($telegramStorageDirectory . '/nasty.madeline');

        /** auth */
//        $telegramAuth = $this->getContainer()->get(TelegramAuth::class);
//        $telegramAuth->setSession('nasty.madeline');
//        $telegramAuth->authorization('79069275941');
//        $telegramAuth->completeAuthorization('28128');

        $address = $telegramStorageDirectory . '/nasty.madeline.sock';
        if (file_exists($address)) {
            unlink($address);
        }
        $socket = new SocketTransport();
        $socket->create(AF_UNIX, SOCK_STREAM, 0);
        $socket->bind($address);
        $socket->listen(10);

        while (true) {
            $connect = $socket->accept();
            if ($streamData = $socket->read($connect, 1024)) {
                $data = json_decode($streamData, true);
                switch ($data['action']) {
                    case 'message.send':
                        $sendResponse = $telegramAPI->sendMessage(...[
                            $data['body']['peerId'],
                            $data['body']['message'],
                        ]);
                        $response = $this->buildResponse(self::RESPONSE_SUCCESS, [
                            'message' => $sendResponse,
                        ]);
                        $socket->write($connect, $response);
                        break;

                    case 'dialogs.get':
                        $dialogs = $telegramAPI->getDialogs();
                        $response = $this->buildResponse(self::RESPONSE_SUCCESS, [
                            'dialogs' => $dialogs
                        ]);
                        $socket->write($connect, $response);
                        break;

                    case 'userInfo.get':
                        $info = $telegramAPI->getFullInfo($data['body']['peerId']);
                        $response = $this->buildResponse(self::RESPONSE_SUCCESS, [
                            'info' => $info
                        ]);
                        $socket->write($connect, $response);
                        break;

                    case 'userPhoto.get':
                        $photo = $telegramAPI->getUserPhoto($data['body']['userId']);
                        $messageMediaPhoto = [
                            '_' => 'messageMediaPhoto',
                            'photo' => $photo,
                            'caption' => '',
                        ];

                        $messageMedia = $telegramAPI->getDownloadInfo($messageMediaPhoto);
                        $fileName = $messageMedia['name'] . $messageMedia['ext'];
                        $imagePath = sprintf(
                            '%s/%s/%s',
                            $telegramImagesPath,
                            $uuid,
                            $fileName
                        );

                        if (file_exists($imagePath)) {
                            $response = $this->buildResponse(self::RESPONSE_SUCCESS, [
                                'file' => sprintf(
                                    '%s/%s/%s',
                                    $telegramImagesUrl,
                                    $uuid,
                                    $fileName
                                ),
                                'cache' => true,
                            ]);
                        } else {
                            $telegramAPI->downloadToDir($messageMediaPhoto, './');
//                            $stream = fopen('php://output', 'w'); // Stream to browser like with echo
//                            $telegramAPI->download_to_stream($messageMediaPhoto, $stream);
                            $response = $this->buildResponse(self::RESPONSE_SUCCESS, [
                                'file' => sprintf(
                                    '%s/%s/%s',
                                    $telegramImagesUrl,
                                    $uuid,
                                    $fileName
                                ),
                                'cache' => false,
                            ]);
                        }
                        $socket->write($connect, $response);
                        break;

                    // List update types - https://docs.madelineproto.xyz/API_docs/types/Update.html
                    case 'longPoll.updates':
                        $updateId = (int)$data['body']['updateId'];
                        $wait = (int)$data['body']['wait'];
                        $response = $this->buildResponse(self::RESPONSE_ERROR, [
                            'description' => 'long poll not responding'
                        ]);
                        for ($i = 0; $i < $wait; $i++) {
                            $updates = $telegramAPI->longPoll($updateId + 1);
                            if (empty($updates)) {
                                sleep(1);
                                continue;
                            }
                            $response = $this->buildResponse(self::RESPONSE_SUCCESS, [
                                'updates' => $updates
                            ]);
                            break;
                        }
                        $socket->write($connect, $response);
                        break;

                    case 'shutdown':
                        unlink($address);
                        exit(0);

                    default:
                        $response = $this->buildResponse(self::RESPONSE_ERROR, [
                            'description' => sprintf('Action "%s" not allowed', $data['action'])
                        ]);
                        $socket->write($connect, $response);
                }
            }
            $socket->shutdown($connect);
            usleep(250000);
        }
        $socket->close();
    }

    /**
     * @param string $type
     * @param array $body
     * @return string
     */
    public function buildResponse(string $type, array $body): string
    {
        return json_encode([
            'status' => $type,
            'body' => $body,
        ]) . PHP_EOL;
    }
}