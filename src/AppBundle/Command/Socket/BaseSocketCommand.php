<?php

namespace AppBundle\Command\Socket;

use AppBundle\Service\Socket\BaseSocket;
use AppBundle\Service\Vk\VkMethodRouter;
use PHPSocketIO\Socket;
use Symfony\Component\Console\Input\InputArgument;
use Workerman\Worker;
use PHPSocketIO\SocketIO;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * // отправить текущему сокету сформировавшему запрос (туда откуда пришла)
 * socket.emit('eventClient', "this is a test");
 * // отправить всем пользователям, включая отправителя
 * io.sockets.emit('eventClient', "this is a test");
 * // отправить всем, кроме отправителя
 * socket.broadcast.emit('eventClient', "this is a test");
 * // отправить всем клиентам в комнате (канале) 'game', кроме отправителя
 * socket.broadcast.to('game').emit('eventClient', 'nice game');
 * // отправить всем клиентам в комнате (канале) 'game', включая отправителя
 * io.sockets.in('game').emit('eventClient', 'cool game');
 * // отправить конкретному сокету, по socketid
 * io.sockets.socket(socketid).emit('eventClient', 'for your eyes only');
 *
 * Class SocketCommand
 * @package AppBundle\Command\Telegram
 */
class BaseSocketCommand extends ContainerAwareCommand
{
    /** @var SocketIO */
    private $io;

    /**
     * @return void
     */
    protected function configure(): void
    {
        $this
            ->setName('socket:handle')
            ->addArgument('action', InputArgument::REQUIRED, 'start/stop/status...')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        global $argv;

        $argv[1] = $input->getArgument('action');
        $this->io = new SocketIO(9001);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $io = $this->io;

        $io->on('connection', function (Socket $socket) use ($io) {
            $vkMethodRouter = $this->getContainer()->get(VkMethodRouter::class);
            $vkMethodRouter->initMethodList($socket, $io);

            $socket->on('disconnect', function () {
                dump('user disconnected');
            });
        });

        Worker::runAll();
    }
}
