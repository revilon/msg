<?php

namespace AppBundle\Dto;

use PHPSocketIO\SocketIO;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class LoginQuery
 * @package AppBundle\Service\Vk\Login
 * @method getHeaders(): array
 * @method getMethod(): string
 * @method getQuery(): array
 */
class BaseQueryDto extends AbstractDto
{
    /** @var string */
    protected $uuid;

    /** @var SocketIO */
    protected $connection;

    /**
     * @param OptionsResolver $options
     * @return void
     */
    protected function configureOptions(OptionsResolver $options): void
    {
        $options->setRequired([
            'connection',
            'method',
            'headers',
            'query',
        ]);

        $options->setNormalizer('headers', function (Options $options, $headers) {
            if (empty($headers['uuid'])) {
                throw new \InvalidArgumentException('empty "uuid".');
            }

            $this->setUuid($headers['uuid']);

            return $headers;
        });

        $options->setAllowedTypes('connection', SocketIO::class);
        $options->setAllowedTypes('method', 'string');
        $options->setAllowedTypes('headers', 'array');
        $options->setAllowedTypes('query', 'array');
    }

    /**
     * @return string
     */
    public function getUuid(): string
    {
        return $this->uuid;
    }

    /**
     * @param string $uuid
     * @return BaseQueryDto
     */
    private function setUuid(string $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * @return SocketIO
     */
    public function getConnection(): SocketIO
    {
        return $this->connection;
    }
}
