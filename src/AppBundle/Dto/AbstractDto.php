<?php

namespace AppBundle\Dto;

use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class AbstractRequest
 * @package AppBundle\Dto
 */
abstract class AbstractDto
{
    /**
     * AbstractRequest constructor.
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        if (!empty($data)) {
            $this->resolve($data);
        }
    }

    /**
     * @param array $data
     * @return void
     */
    public function resolve(array $data): void
    {
        $options = new OptionsResolver();
        $this->configureOptions($options);

        $options->resolve($data);

        foreach ($options->getDefinedOptions() as $prop) {
            $this->$prop = $data[$prop];
        }
    }

    /**
     * @param OptionsResolver $options
     * @return void
     */
    abstract protected function configureOptions(OptionsResolver $options): void;

    /**
     * @param $method
     * @param $parameters
     * @return mixed
     */
    public function __call($method, $parameters)
    {
        if (method_exists($this, $method)) {
            return \call_user_func_array([$this, $method], $parameters);
        }
        if (strpos($method, 'set') === 0) {
            $property = lcfirst(substr($method, 3));
            return $this->$property = current($parameters);
        }
        if (strpos($method, 'get') === 0) {
            $property = lcfirst(substr($method, 3));
            return $this->$property;
        }
        return user_error(sprintf('undefined method "%s"', $method));
    }
}
